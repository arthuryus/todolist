<?php
namespace app\web;

use app\App;
use app\base\Helper;

/**
 * Class app\web\Router
 */
class Router
{
    /**
     * @var self
     */
    private static $_instance = null;

    private function __construct()
    {

    }

    /**
     * @return self
     */
    public static function getInstance():self
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * @return string|boolean
     */
    public function run():mixed
    {
        $router = App::getConfig('router');
        $requestPath = App::$app->request->path;
        $checkAuth = false;

        /* preparing router configuration */
        if (array_key_exists($requestPath, $router)) {
            $routerPath = $router[$requestPath];

            if (is_array($routerPath)) {
                $path = $routerPath[0];
                $checkAuth = $routerPath[1];
            }
            else {
                $path = $routerPath;
            }
        }
        else {
            $path = trim($requestPath, '/');
        }

        $paths = explode('/', $path);


        /* if authorization and opening login page
         * redirect to index page
         * */
        if (App::$app->isAuth() && in_array($path, [App::getConfig('default')['authRoute'], 'auth/registration'])) {
            return (new Controller())->redirect('/');
        }

        /* if current path needs authorization
         * redirect to login page
         * */
        if ($checkAuth && !App::$app->isAuth()) {
            if (App::$app->request->isAjax) {
                return (new Controller())->renderAjax(['success' => false, 'redirect' => Helper::getUrlFull('/' . App::getConfig('default')['authRoute'])]);
            }
            else {
                return (new Controller())->redirect('/' . App::getConfig('default')['authRoute']);
            }
        }

        /* search the right Controller/Action
        * */
        if (count($paths) <= 2) {
            $defaultPaths = explode('/', App::getConfig('default')['defaultRoute']);

            try {
                if ($content = $this->runControllerAction($paths[0] ? : $defaultPaths[0], $paths[1] ? : $defaultPaths[1])) {
                    return $content;
                }
            }
            catch (\Exception $exception) {
                if ($content = $this->runControllerActionError($exception->getCode(), $exception->getMessage())) {
                    return $content;
                }
            }
        }

        /* if the right Controller/Action was not found and there was no error
         * then run Not Fund page
        * */
        if ($content = $this->runControllerActionError(404, 'Page note found.')) {
            return $content;
        }

        return false;
    }

    /**
     * @param string $controllerName
     * @param string $actionName
     * @param array $params
     * @return string|boolean
     */
    private function runControllerAction(string $controllerName, string $actionName, array $params=[]):mixed
    {
        $controller = Controller::getControllerFullName($controllerName);
        if (class_exists($controller)) {
            $controllerClass = new $controller();

            $action = $controllerClass->getActionFullName($actionName);
            if (method_exists($controllerClass, $action)) {
                //return $controllerClass->$action(...$params);
                return call_user_func_array([$controllerClass, $action], $params);
            }
        }

        return false;
    }

    /**
     * @param int $code
     * @param string $message
     * @return string|boolean
     */
    private function runControllerActionError(int $code, string $message):mixed
    {
        $errorPaths = explode('/', App::getConfig('default')['errorRoute']);

        if ($content = $this->runControllerAction($errorPaths[0], $errorPaths[1], ['message' => $message])) {//[$message]
            App::$app->response->setError($code, $message);

            return $content;
        }

        return false;
    }
}
