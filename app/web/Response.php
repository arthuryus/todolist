<?php
namespace app\web;

use app\base\Helper;

/**
 * Class app\web\Response
 *
 * @property string $_format
 * @property string $_content
 * @property int $_statusCode
 * @property string $_statusMessage
 */
class Response
{
    private string $_format;

    private string $_content;

    private int $_statusCode = 200;

    private string $_statusMessage;

    const FORMAT_HTML = 'html';
    const FORMAT_JSON = 'json';

    /**
     * @var self
     */
    private static $_instance = null;

    /**
     * @param string $content
     */
    private function __construct(string $content='')
    {
        $this->setFormat(self::FORMAT_HTML);
        $this->setContent($content);
    }

    /**
     * @return self
     */
    public static function getInstance():self
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * @param string $format
     * @return void
     */
    public function setFormat(string $format):void
    {
        $this->_format = $format;
    }

    /**
     * @return void
     */
    public function setFormatHtml():void
    {
        $this->setFormat(self::FORMAT_HTML);
    }

    /**
     * @return void
     */
    public function setFormatJson():void
    {
        $this->setFormat(self::FORMAT_JSON);
    }

    /**
     * @param string $content
     * @return void
     */
    public function setContent(string $content):void
    {
        $this->_content = $content;
    }

    /**
     * @param int $code
     * @param string $message
     * @return void
     */
    public function setError(int $code, string $message):void
    {
        $this->_statusCode = $code;
        $this->_statusMessage = $message;
    }

    /**
     * @return void
     */
    public function output():void
    {
        if ($this->_format === self::FORMAT_JSON) {
            header('Content-Type: application/json');
        }

        if ($this->_statusCode === 404) {
            header("HTTP/1.0 404 Not Found");
        }

        echo $this->_content;
    }

    /**
     * @param string $action
     * @param array $params
     * @return void
     */
    public function redirect(string $action, array $params=[]):void
    {
        header('Location: ' . Helper::getUrlFull($action, $params));
        exit();
    }


}
