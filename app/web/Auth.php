<?php
namespace app\web;

use app\App;
use common\models\User;

/**
 * Class app\web\Application
 *
 * @property User $user
 */
class Auth
{
    public User $user;

    /**
     * @var self $_instance
     */
    private static $_instance;

    private function __construct()
    {

    }

    /**
     * @return self
     */
    public static function getInstance():self
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * @return void
     * @throws \ReflectionException
     */
    public function init():void
    {
        if ($user = App::$app->session->getValue('user')) {
            $this->user = new User();
            $this->user->setAttributes($user);
        }
    }

    /**
     * @return bool
     */
    public function isAuth():bool
    {
        return isset($this->user);
    }

    /**
     * @param User $user
     * @return bool
     * @throws \ReflectionException
     */
    public function login(User $user):bool
    {
        if ($user) {
            $user->update(['logged_at' => strtotime('now')]);
            $userData = $user->toArray();
            unset($userData['password']);

            App::$app->session->setValue('user', $userData);

            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function logout():bool
    {
        App::$app->session->removeValue('user');

        return true;
    }
}
