<?php
namespace app\web;

use app\App;

/**
 * Class app\web\Application
 *
 * @property Session $session
 * @property Request $request
 * @property Response $response
 * @property Router $router
 * @property Auth $auth
 */
class Application extends \app\base\Application
{
    public Session $session;

    public Request $request;

    public Response $response;

    public Router $router;

    public Auth $auth;

    /**
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $config)
    {
        parent::__construct($config);

        $this->session = Session::getInstance();
        $this->request = Request::getInstance();
        $this->response = Response::getInstance();
        $this->router = Router::getInstance();
        $this->auth = Auth::getInstance();
    }

    /**
     * @return void
     */
    public function init():void
    {
        App::$app = $this;

        App::$app->auth->init();
    }

    /**
     * @return void
     */
    public function run():void
    {
        $this->init();

        if ($content = $this->router->run()) {
            App::$app->response->setContent($content);
            App::$app->response->output();
        }
        else {
            header("HTTP/1.0 404 Not Found");
        }
    }

    /**
     * @return bool
     */
    public function isAuth():bool
    {
        return $this->auth->isAuth();
    }
}
