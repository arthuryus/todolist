<?php
namespace app\web;

/**
 * Class app\web\View
 *
 * @property string $title
 * @property string $_namespace
 * @property string $_folderMain
 * @property string $_folderLayout
 */
class View
{
    public string $title;

    private string $_namespace;

    private string $_folderMain;

    private string $_folderLayout;

    public function __construct(string $_namespace, string $_folderMain='views', string $_folderLayout='layouts')
    {
        $this->_namespace = $_namespace;
        $this->_folderMain = $_folderMain;
        $this->_folderLayout = $_folderLayout;
    }

    /**
     * @param string $_name_
     * @param array $_data_
     * @return string
     */
    public function renderFile(string $_name_, array $_data_ = []):string
    {
        if (file_exists($_file_ = $this->getFileFullPath($_name_))) {
            ob_start();
            ob_implicit_flush(false);
            extract($_data_, EXTR_OVERWRITE);
            require($_file_);

            return ob_get_clean();
        }
        else {
            return 'View file doesn\'t exist: '.$_file_;
        }
    }

    /**
     * @param string $_name_
     * @param array $_data_
     * @return string
     */
    public function renderLayout(string $_name_, array $_data_=[]):string
    {
        return $this->renderFile($this->_folderLayout . '/' . $_name_, $_data_);
    }

    /**
     * @param string $_name_
     * @param array $_data_
     * @return string
     */
    public function renderContent(string $_name_, array $_data_=[]):string
    {
        return $this->renderFile($_name_, $_data_);
    }

    /**
     * @param string $_layout_
     * @param string $_name_
     * @param array $_data_
     * @return string
     */
    public function render(string $_layout_, string $_name_, array $_data_=[]):string
    {
        return $this->renderLayout($_layout_, [
            'content' => $this->renderContent($_name_, $_data_)
        ]);
    }

    /**
     * @param array $_data_
     * @return string
     */
    public function renderAjax(array $_data_=[]):string
    {
        return json_encode($_data_);
    }

    /**
     * @param string $_name_
     * @return string
     */
    public function getFileFullPath(string $_name_):string
    {
        return DIRECTORY_ROOT . implode('/', [$this->_namespace, $this->_folderMain, $_name_ . '.php']);
    }
}
