<?php
namespace app\web;

/**
 * Class app\web\Request
 *
 * @property array $server
 * @property array $get
 * @property array $post
 * @property array $input
 * @property string $path
 * @property bool $isAjax
 *
 * @property string $host
 * @property string $proto
 * @property string $method
 * @property string $origin
 * @property string $url
 */
class Request
{
    public array $server;
    public array $get;
    public array $post;
    public array $input;
    public string $path;
    public bool $isAjax;

    public string $host;
    public string $proto;
    public string $method;
    public string $origin;
    public string $url;

    /**
     * @var self $_instance
     */
    private static $_instance;

    private function __construct()
    {
        $this->server = $_SERVER;

        $this->get = $_GET;

        $this->post = $_POST;

        $this->input = (array)json_decode(file_get_contents('php://input'), true);

        $this->path = parse_url($_SERVER['REQUEST_URI'])['path'];

        $this->isAjax = strtolower($_SERVER['HTTP_X_REQUESTED_WITH'] ? : '') == 'xmlhttprequest' ? true : false;

        $this->host = $_SERVER['HTTP_HOST'];

        $this->proto = $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || $_SERVER['HTTPS'] == 'on' || stripos($_SERVER['HTTP_CF_VISITOR'] ? : '', '"scheme":"https"') !== false ? 'https' : 'http';

        $this->method = $_SERVER['REQUEST_METHOD'];

        $this->origin = $this->proto.'://'.$this->host.'/';

        $this->url = rtrim($this->origin, '/').$this->path;
    }

    /**
     * @return self
     */
    public static function getInstance():self
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
}
