<?php
namespace app\web;

/**
 * Class app\web\Session
 */
class Session
{
    /**
     * @var self $_instance
     */
    private static $_instance;

    private function __construct()
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
    }

    /**
     * @return self
     */
    public static function getInstance():self
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * @param string $key
     * @param mixed $val
     * @return void
     */
    public function setValue(string $key, mixed $val):void
    {
        $_SESSION[$key] = $val;
    }

    /**
     * @param string $key
     * @return mixed $user
     */
    public function getValue(string $key):mixed
    {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }

    /**
     * @param string $key
     * @return void
     */
    public function removeValue(string $key)
    {
        unset($_SESSION[$key]);
    }
}
