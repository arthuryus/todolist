<?php
namespace app\web;

use app\App;
use app\base\Helper;

/**
 * Class app\web\Controller
 *
 * @property string $defaultAction
 * @property string $layout
 * @property View $_view
 */
class Controller
{
    public string $defaultAction = 'index';

    public string $layout = 'main';

    private View $_view;

    /**
     * @param string $name
     * @return string
     */
    public static function getControllerFullName(string $name):string
    {
        return App::getConfig('default')['controllerNamespace'] . '\\' . Helper::toCamelCase($name) . 'Controller';
    }

    /**
     * @param string $name
     * @return string
     */
    public function getActionFullName(string $name):string
    {
        return 'action' . Helper::toCamelCase($name ? : $this->defaultAction);
    }

    /**
     * @return View
     */
    public function getView():View
    {
        if (!isset($this->_view)) {
            $this->_view = new View(dirname(App::getConfig('default')['controllerNamespace']));
        }

        return $this->_view;
    }

    /**
     * @param string $name
     * @param array $data
     * @return string
     */
    public function render(string $name, array $data=[]):string
    {
        $content = $this->getView()->render($this->layout, $name, $data);

        return $content;
    }

    /**
     * @param array $data
     * @return string
     */
    public function renderAjax(array $data=[]):string
    {
        App::$app->response->setFormatJson();

        $content = $this->getView()->renderAjax($data);

        return $content;
    }

    /**
     * @param string $action
     * @param array $params
     * @return void
     */
    public function redirect(string $action, array $params=[]):void
    {
        App::$app->response->redirect($action, $params);
    }
}
