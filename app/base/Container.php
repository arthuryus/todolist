<?php
namespace app\base;

use app\App;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Container\ContainerExceptionInterface;

/**
 * Class app\base\Container
 *
 * @property array $_entries
 */
class Container implements ContainerInterface
{
    private array $_entries = [];

    /**
     * @var self
     */
    private static Container $_instance;

    private function __construct()
    {

    }

    /**
     * @return self
     */
    public static function getInstance():self
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function get(string $id):mixed
    {
        if ($this->has($id)) {
            return $this->_entries[$id];
        }

        $containers = (array)App::getConfig('containers');
        if (array_key_exists($id, $containers)) {
            $this->_entries[$id] = new $containers[$id]['class']();

            return $this->_entries[$id];
        }

        return null;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function has(string $id):bool
    {
        return isset($this->_entries[$id]);
    }
}
