<?php
namespace app\base;

use app\App;

/**
 * Class app\base\Helper
 */
class Helper
{
    /**
     * @param array $replace
     * @return string
     */
    public static function getPaginationUrl(array $replace=[]):string
    {
        $uriParse = parse_url(App::$app->request->server['REQUEST_URI']);
        $uriPath = $uriParse['path'];
        $uriQueryArray = [];

        if ($uriQuery = $uriParse['query']) {
            parse_str($uriQuery, $uriQueryArray);
        }

        foreach ($replace as $key => $val) {
            $uriQueryArray[$key] = $val;
        }
        $uriQuery = http_build_query($uriQueryArray);

        return implode('?', array_filter([$uriPath, $uriQuery]));
    }

    /**
     * @param string $action
     * @param array $params
     * @return string
     */
    public static function getUrl(string $action, array $params=[]):string
    {
        return implode('?', array_filter([$action, http_build_query($params)]));
    }

    /**
     * @param string $action
     * @param array $params
     * @return string
     */
    public static function getUrlFull(string $action, array $params=[]):string
    {
        return rtrim(App::$app->request->origin, '/') . self::getUrl($action, $params);
    }

    /**
     * @param string $value
     * @return string
     */
    public static function cleanValue(string $value):string
    {
        return htmlspecialchars(strip_tags(trim($value)));
    }

    /**
     * @param string $value
     * @return string
     */
    public static function toCamelCase(string $value):string
    {
        return implode('', array_map('ucfirst', array_filter(explode('-', $value))));
    }
}
