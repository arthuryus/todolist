<?php
namespace app\base;

/**
 * Class app\base\Model
 *
 * @property array $_errors
 */
class Model
{
    private $_errors = [];

    /**
     * @param string $attribute
     * @param string $message
     * @return void
     */
    public function setErrors(string $attribute, string $message):void
    {
        $this->_errors[$attribute][] = $message;
    }

    /**
     * @param string $attribute
     * @return array
     */
    public function getErrors(?string $attribute=null):array
    {
        return ($attribute === null) ? $this->_errors : (array)$this->_errors[$attribute];
    }

    /**
     * @param string $attribute
     * @return bool
     */
    public function hasErrors(?string $attribute=null):bool
    {
        return ($attribute === null) ? !empty($this->_errors) : !empty((array)$this->_errors[$attribute]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function formName():string
    {
        $reflector = new \ReflectionClass($this);

        return $reflector->getShortName();
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function attributes():array
    {
        $class = new \ReflectionClass($this);
        $names = [];
        foreach ($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            if (!$property->isStatic()) {
                $names[] = $property->getName();
            }
        }

        return $names;
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function fields():array
    {
        $fields = $this->attributes();

        return array_combine($fields, $fields);
    }

    /**
     * @param array $names
     * @param array $except
     * @return array
     * @throws \ReflectionException
     */
    public function getAttributes(?array $names = null, array $except = []):array
    {
        $values = [];
        if ($names === null) {
            $names = $this->attributes();
        }
        foreach ($names as $name) {
            $values[$name] = $this->$name;
        }
        foreach ($except as $name) {
            unset($values[$name]);
        }

        return $values;
    }

    /**
     * @param array $values
     * @return void
     * @throws \ReflectionException
     */
    public function setAttributes(array $values):void
    {
        if (is_array($values)) {
            $attributes = array_flip($this->attributes());
            foreach ($values as $name => $value) {
                if (isset($attributes[$name])) {
                    $this->$name = $value;
                }
            }
        }
    }

    /**
     * @param array $data
     * @param string|null $formName
     * @return bool
     * @throws \ReflectionException
     */
    public function load(array $data, ?string $formName = null):bool
    {
        $scope = $formName === null ? $this->formName() : $formName;
        if ($scope === '' && !empty($data)) {
            $this->setAttributes($data);

            return true;
        } elseif (isset($data[$scope])) {
            $this->setAttributes($data[$scope]);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function toArray():array
    {
        $values = [];
        $attributes = $this->attributes();
        foreach ($attributes as $name) {
            $values[$name] = $this->$name;
        }

        return $values;
    }
}
