<?
namespace app\base;

/**
 * Class app\base\Database
 *
 * @property \PDO $_connection
 */
class Database
{
    private \PDO $_connection;

    /**
     * @var self
     */
    private static $_instance;

    /**
     * @param array $config
     * @throws \Exception
     */
    private function __construct(array $config)
    {
        $this->connect($config['dsn'], $config['username'], $config['password'], $config['charset']);
    }

    /**
     * @param array $config
     * @throws \Exception
     * @return self
     */
    public static function getInstance(array $config):self
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new self($config);
        }

        return self::$_instance;
    }

    /**
     * @param string $dsn
     * @param string $username
     * @param string $password
     * @param string $charset
     * @throws \Exception.
     * @return void
     */
    private function connect(string $dsn, string $username, string $password, string $charset):void
    {
        try {
            $this->_connection = new \PDO(implode(';', [$dsn, $charset]), $username, $password);
        }
        catch(\PDOException $e) {
            throw new \Exception("Error connecting to database");
        }
    }

    /**
     * @param string $query
     * @param array $params
     * @param int $flags
     * @param string $arg
     * @param string $args
     * @return array|boolean
     */
    public function query(string $query, array $params=[], ?int $flags=0, ?string $arg=null, ?string $args=null)
    {
        if(is_array($params) && count($params)) {
            $res = $this->_connection->prepare($query);
            $res->execute(array_map(function($v) { return (string)$v; }, $params));

            return $res;
        }

        return call_user_func_array([$this->_connection, 'query'], $args !== null ? [$query, $flags, $arg, $args] : ($arg !== null ? [$query, $flags, $arg] : [$query, $flags]));
    }

    /**
     * @param string $val
     * @return string|boolean
     */
    public function sql($val)
    {
        return $this->_connection->quote($val);
    }

    /**
     * @param string $query
     * @param array $params
     * @return array|boolean
     */
    public function row($query, $params = [], $flags = \PDO::FETCH_ASSOC)
    {
        return $this->query($query, $params)->fetch($flags);
    }

    /**
     * @param string $query
     * @param array $params
     * @return array|boolean
     */
    public function rows($query, $params = [], $flags = \PDO::FETCH_ASSOC, $arg = null, $args = null)
    {
        return call_user_func_array([$this->query($query, $params), 'fetchAll'], $args !== null ? [$flags, $arg, $args] : ($arg !== null ? [$flags, $arg] : [$flags]));
    }

    /**
     * @param string $query
     * @param array $params
     * @param integer $colNumber
     * @return string
     */
    public function val($query, $params = [], $colNumber = 0)
    {
        return $this->query($query, $params)->fetchColumn($colNumber);
    }

    /**
     * @param string $table
     * @param array $row
     * @param boolean $getInsertRow
     * @return array|boolean
     */
    public function insert($table, $row, $getInsertRow = false)
    {
        if(is_string($table) && trim($table) && is_array($row) && count($row)) {
            if($this->query("INSERT INTO `$table` SET `".implode('` = ?, `', array_keys($row))."` = ?", array_values($row)) !== false) {
                return $getInsertRow ? $this->row("SELECT * FROM `$table` WHERE `id` = ?", [$this->connection->lastInsertId()]) : (int)$this->connection->lastInsertId();
            }
        }

        return false;
    }

    /**
     * @param string $table
     * @param array $row
     * @param array $where
     * @param boolean $getUpdateRow
     * @return array|boolean
     */
    public function update($table, $row, $where, $getUpdateRow = false)
    {
        if(is_string($table) && trim($table) && is_array($row) && count($row)) {
            if(is_array($where) && count($where)) {
                if(($res = $this->query("UPDATE `$table` SET `".implode('` = ?, `', array_keys($row))."` = ? WHERE `".implode('` = ? AND `', array_keys($where)).'` = ?', array_merge(array_values($row), array_values($where)))) !== false) {
                    return $res->rowCount();
                }
            }
            else if(is_numeric($where) && (int)$where > 0) {
                if(($res = $this->query("UPDATE `$table` SET `".implode('` = ?, `', array_keys($row))."` = ? WHERE `id` = ?", array_merge(array_values($row), [$where]))) !== false) {
                    return $getUpdateRow ? $this->row("SELECT * FROM `$table` WHERE `id` = ?", [$where]) : $res->rowCount();
                }
            }
        }

        return false;
    }

    /**
     * @param string $table
     * @param array $where
     * @return array|boolean
     */
    public function delete($table, $where)
    {
        if(is_string($table) && trim($table)) {
            if(is_array($where) && count($where)) {
                if(($res = $this->query("DELETE FROM `$table` WHERE `".implode('` = ? AND `', array_keys($where)).'` = ?', array_values($where))) !== false) {
                    return $res->rowCount();
                }
            }
            else if(is_numeric($where) && (int)$where > 0) {
                if(($res = $this->query("DELETE FROM `$table` WHERE `id` = ?", [$where])) !== false) {
                    return $res->rowCount();
                }
            }
        }

        return false;
    }

    /**
     * @param string $table
     * @return array
     */
    public function getTableSchema($table)
    {
        if(is_string($table) && trim($table)) {
            return $this->rows("DESCRIBE {$table}");
        }

        return [];
    }

    /**
     * @param string $table
     * @return array
     */
    public function getColumns($table)
    {
        return array_column($this->getTableSchema($table), 'Field');
    }

    /**
     * @param string $table
     * @return array
     */
    public function getPrimaryKeys($table)
    {
        $keys = [];
        foreach ($this->getTableSchema($table) as $row) {
            if ($row['Key'] === 'PRI' && $row['Extra'] === 'auto_increment') {
                $keys[] = $row['Field'];
            }
        }

        return $keys;

        /*if(is_string($table) && trim($table)) {
            return Application::$app->db->rows("SHOW KEYS FROM `{$table}` WHERE `Key_name` = 'PRIMARY'");
        }*/
    }
}
