<?php
namespace app\base;

use app\App;

/**
 * Class app\base\DatabaseModel
 *
 * @property array $_oldAttributes
 */
class DatabaseModel extends Model
{
    private array $_oldAttributes;

    /**
     * @return string
     */
    public static function tableName():string
    {
        return strtolower(end(explode('\\', get_called_class())));
    }

    /**
     * @return array
     */
    public function attributes():array
    {
        return App::$app->db->getColumns(static::tableName());
    }

    /**
     * @param array $values
     * @return void
     */
    public function setOldAttributes(array $values):void
    {
        $this->_oldAttributes = $values;
    }

    /**
     * @return array
     */
    public function getOldAttributes():array
    {
        return $this->_oldAttributes;
    }

    /**
     * @param array $data
     * @param string|null $formName
     * @return bool
     */
    public function load(array $data, ?string $formName = null):bool
    {
        $this->setOldAttributes($this->toArray());

        if (parent::load($data, $formName)) {
            return true;
        }

        $this->setOldAttributes([]);

        return false;
    }

    /**
     * @param array $where
     * @param array $orderBy
     * @param array $limit
     * @param array|string $fields
     * @return self|null
     */
    public static function findOne(array $where=[], array $orderBy=[], array $limit=[], $fields="*"):?self
    {
        [$query, $params] = static::prepareQuerySelect($where, $orderBy, $limit, $fields);

        if ($row = App::$app->db->row($query, $params)) {
            $className = get_called_class();

            $model = new $className();
            $model->setAttributes($row);

            return $model;
        }

        return null;
    }

    /**
     * @param array $where
     * @param array $orderBy
     * @param array $limit
     * @param array|string $fields
     * @return self[]|null
     */
    public static function findAll(array $where=[], array $orderBy=[], array $limit=[], $fields="*")
    {
        [$query, $params] = static::prepareQuerySelect($where, $orderBy, $limit, $fields);

        $result = [];
        if ($rows = App::$app->db->rows($query, $params)) {
            $className = get_called_class();

            foreach ($rows as $row) {
                $model = new $className();
                $model->setAttributes($row);

                $result[] = $model;
            }
        }

        return $result;
    }

    /**
     * @param array $where
     * @param array $orderBy
     * @param array $limit
     * @return string
     */
    public static function getCount(array $where=[], array $orderBy=[], array $limit=[]):string
    {
        [$query, $params] = static::prepareQuerySelect($where, $orderBy, $limit, 'COUNT(*)');

        return App::$app->db->val($query, $params);
    }

    /**
     * @return self|null
     */
    public function insert():?self
    {
        $row = $this->toArray();
        $row = array_filter($row, function($v) { return !is_null($v); });
        foreach (App::$app->db->getPrimaryKeys(static::tableName()) as $primaryKey) {
            unset($row[$primaryKey]);
        }

        if ($result = App::$app->db->insert(static::tableName(), $row, true)) {
            $this->setAttributes($result);
        }

        return null;
    }

    /**
     * @param array $row
     * @param array $where
     * @return self
    */
    public function update(array $row=[], array $where=[]):self
    {
        $rowDefault = $this->toArray();
        if (!$row) {
            $row = $rowDefault;
        }
        else {
            foreach ($row as $key => $val) {
                $this->{$key} = $val;
            }
        }

        if (!$where) {
            foreach (App::$app->db->getPrimaryKeys(static::tableName()) as $primaryKey) {
                $where[$primaryKey] = $rowDefault[$primaryKey];
                unset($row[$primaryKey]);
            }
        }

        if (App::$app->db->update(self::tableName(), $row, $where)) {
            $this->setAttributes($where);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function delete():bool
    {
        $where = [];

        $row = $this->toArray();
        foreach (App::$app->db->getPrimaryKeys(static::tableName()) as $primaryKey) {
            $where[$primaryKey] = $row[$primaryKey];
        }

        if (App::$app->db->delete(self::tableName(), $where)) {
            $this->_oldAttributes = null;
            //unset($this);

            return true;
        }

        return false;
    }

    /**
     * @param array $where
     * @param array $orderBy
     * @param array $limit
     * @param array|string $fields
     * @return array
     */
    private static function prepareQuerySelect(array $where=[], array $orderBy=[], array $limit=[], $fields="*"):array
    {
        $fields = implode(', ', (array)$fields);

        $query = "SELECT {$fields} FROM `".self::tableName()."`";

        $params = ($queryWhere = self::prepareQueryWhere($where)) ? $where[1] : [];
        $queryOrderBy = self::prepareQueryOrderBy($orderBy);
        $queryLimit = self::prepareQueryLimit($limit);

        return [implode(' ',array_filter([$query, $queryWhere, $queryOrderBy, $queryLimit])), $params];
    }

    /**
     * @param array $where
     * @return string
     */
    private static function prepareQueryWhere(array $where=[]):string
    {
        $queryWhere = "";
        if ($where && count($where) == 2) {
            $queryWhere = "WHERE " . $where[0];
        }

        return $queryWhere;
    }

    /**
     * @param array $orderBy
     * @return string
     */
    private static function prepareQueryOrderBy(array $orderBy=[]):string
    {
        $queryOrderBy = "";
        if ($orderBy && count($orderBy)) {
            foreach ($orderBy as $key => $val) {
                $queryOrderBy .= "ORDER BY `{$key}` {$val}";
            }
        }

        return $queryOrderBy;
    }

    /**
     * @param array $limit
     * @return string
     */
    private static function prepareQueryLimit(array $limit=[]):string
    {
        $queryLimit = "";
        if ($limit && count($limit) == 2) {
            $queryLimit = "LIMIT {$limit[0]}, {$limit[1]}";
        }

        return $queryLimit;
    }
}
