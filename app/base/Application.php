<?php
namespace app\base;

use app\App;

/**
 * Class app\base\Application
 *
 * @property Database $db
 * @property Container $container
 * @property array $_config
 */
class Application
{
    public Database $db;

    public Container $container;

    private array $_config;

    /**
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $config)
    {
        $this->setConfig($config);

        $this->db = Database::getInstance(static::getConfig('db'));
        $this->container = Container::getInstance();
    }

    /**
     * @return void
     */
    public function init():void
    {
        App::$app = $this;
    }

    /**
     * @param array $config
     */
    private function setConfig(array $config):void
    {
        if (!isset($this->_config)) {
            $this->_config = $config;
        }
    }

    /**
     * @param string $name
     * @return array|string
     */
    public function getConfig(string $name=''):mixed
    {
        return $name ? $this->_config[$name] : $this->_config;
    }

    /**
     * @return void
     */
    public function run():void
    {
        $this->init();
    }
}
