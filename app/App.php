<?php
namespace app;

use app\base\Application;

/**
 * Class app\App
 */
class App
{
    /**
     * @var Application
     */
    public static Application $app;

    /**
     * @param string $name
     * @return mixed
     */
    public static function getConfig(string $name=''):mixed
    {
        if (static::$app) {
            return static::$app->getConfig($name);
        }

        return null;
    }
}
