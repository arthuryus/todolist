<?php
namespace tests\unit;

use app\App;
use PHPUnit\Framework\TestCase;
use frontend\models\LoginForm;
use frontend\models\User;
use frontend\services\auth\AuthService;

class AuthTest extends TestCase
{
    private static $_dataLogin = [
        ['expected' => false, 'data' => ['username' => '', 'password' => '']],
        ['expected' => false, 'data' => ['username' => 'not_exists', 'password' => '']],
        ['expected' => false, 'data' => ['username' => '', 'password' => 'not_exists']],
        ['expected' => false, 'data' => ['username' => 'not_exists', 'password' => 'not_exists']],
        ['expected' => false, 'data' => ['username' => 'admin', 'password' => '']],
        ['expected' => false, 'data' => ['username' => '', 'password' => '123']],
        ['expected' => true, 'data' => ['username' => 'admin', 'password' => '123']],
    ];

    /**
     * @var AuthService
    */
    private AuthService $authService;

    /**
     * @return AuthService
     */
    private function getAuthService()
    {
        return App::$app->container->get('AuthService');
    }

    protected function setUp(): void
    {
        $this->authService = $this->getAuthService();
    }

    public function testLogin()
    {
        foreach (static::$_dataLogin as $item) {
            $model = new LoginForm();
            $model->load($item['data'], '');

            $expected = $item['expected'];
            $result = $model->validate();

            $this->assertTrue($expected === $result);

            if ($expected === true) {
                $user = $model->getUser();
                $this->assertInstanceOf(User::class, $user);
            }
        }
    }


}
