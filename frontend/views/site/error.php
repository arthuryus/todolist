<?php
/* @var $text String */

$this->title = 'Not Found';
?>
<div class="alert alert-danger"><?= $text ?></div>
