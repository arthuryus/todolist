<?php
use app\App;

$alert = App::$app->session->getValue('alert');
App::$app->session->removeValue('alert');
?>
<? if ($alert) { ?>
    <div class="alert alert-<?= $alert['type'] ?>"><?= $alert['message'] ?></div>
<? } ?>
