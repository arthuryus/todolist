<?php
/* @var $content String */

use app\App;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $this->title?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <link href="/assets/css/styles.css" rel="stylesheet" />
</head>
<body>
    <div class="header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a href="/" class="navbar-brand">Todo List</a>
                <div>
                    <a href="/create" class="btn btn-primary">Create</a>
                    <? if (App::$app->isAuth()) { ?>
                        <a href="/auth/logout" class="btn btn-primary">Logout (<?= App::$app->auth->user->username ?>)</a>
                    <? } else { ?>
                        <a href="/auth/login" class="btn btn-primary">Login</a>
                    <? } ?>
                </div>
            </div>
        </nav>
    </div>
    <div class="container p-3">
        <?= $this->renderContent('site/_alert', [

        ]) ?>

        <?php echo $content ?>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="/assets/js/scripts.js"></script>
</body>
</html>
