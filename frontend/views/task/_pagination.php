<?php
/* @var $page integer */
/* @var $length integer */

use app\base\Helper;
?>
<? if ($length > 1) { ?>
    <nav aria-label="...">
        <ul class="pagination">
            <li class="page-item<?= $page == 1 ? ' disabled' : '' ?>">
                <a href="<?= $page == 1 ? '#' : Helper::getPaginationUrl(['page' => $page-1]) ?>" class="page-link">Previous</a>
            </li>
            <? for ($i = 1; $i <= $length; $i++) { ?>
                <? if ($page == $i) { ?>
                    <li class="page-item active">
                        <a href="<?= Helper::getPaginationUrl(['page' => $i]) ?>" class="page-link"><?= $i ?></a>
                    </li>
                <? } else { ?>
                    <li class="page-item">
                        <a href="<?= Helper::getPaginationUrl(['page' => $i]) ?>" class="page-link"><?= $i ?></a>
                    </li>
                <? } ?>
            <? } ?>
            <li class="page-item<?= $page == $length ? ' disabled' : '' ?>">
                <a href="<?= $page == $length ? '#' : Helper::getPaginationUrl(['page' => $page + 1]) ?>" class="page-link">Next</a>
            </li>
        </ul>
    </nav>
<? } ?>
