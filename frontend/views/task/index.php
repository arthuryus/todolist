<?php
/* @var $models frontend\models\Task[] */
/* @var $editedList array */
/* @var $statusList array */
/* @var $sort String */
/* @var $page Integer */
/* @var $length Integer */
/* @var $isAuth boolean */

$this->title = 'Task List';
?>
<div class="task-list">
    <h1><?= $this->title ?></h1>

    <?= $this->renderContent('task/_list', [
        'models' => $models,
        'editedList' => $editedList,
        'statusList' => $statusList,
        'sort' => $sort,
        'isAuth' => $isAuth,
    ]) ?>

    <?= $this->renderContent('task/_pagination', [
        'page' => $page,
        'length' => $length,
    ]) ?>

</div>
