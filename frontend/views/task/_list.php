<?php
/* @var $models frontend\models\Task[] */
/* @var $editedList array */
/* @var $statusList array */
/* @var $sort String */
/* @var $isAuth boolean */

use app\base\Helper;
?>
<table class="table task-list-table">
    <thead>
    <tr>
        <th scope="col"><a href="<?= Helper::getPaginationUrl(['sort' => ($sort == 'username') ? '-username' : 'username']) ?>">Username</a></th>
        <th scope="col"><a href="<?= Helper::getPaginationUrl(['sort' => ($sort == 'email') ? '-email' : 'email']) ?>">Email</a></th>
        <th scope="col"><a href="<?= Helper::getPaginationUrl(['sort' => ($sort == 'text') ? '-text' : 'text']) ?>">Text</a></th>
        <? if ($isAuth) { ?>
            <th scope="col"><a href="<?= Helper::getPaginationUrl(['sort' => ($sort == 'edited') ? '-edited' : 'edited']) ?>">Edited</a></th>
        <? } ?>
        <th scope="col" class="w-10"><a href="<?= Helper::getPaginationUrl(['sort' => ($sort == 'status') ? '-status' : 'status']) ?>">Status</a></th>
        <th scope="col" class="w-10"></th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($models as $index => $model) { ?>
        <?= $this->renderContent('task/_item', [
            'model' => $model,
            'editedList' => $editedList,
            'statusList' => $statusList,
            'isAuth' => $isAuth,
        ]) ?>
    <? } ?>
    </tbody>
</table>
