<?php
/* @var $model frontend\models\Task */
/* @var $editedList array */
/* @var $statusList array */

$this->title = 'Task View';
?>
<div class="task-view">

    <h1><?= $this->title . '#' . $model->id ?></h1>

    <dl>
        <dt>Username</dt>
        <dd><?= htmlspecialchars($model->username) ?></dd>
        <dt>Email</dt>
        <dd><?= htmlspecialchars($model->email) ?></dd>
        <dt>Text</dt>
        <dd><?= htmlspecialchars($model->text) ?></dd>
        <dt>Edited</dt>
        <dd><span class="badge <?= $model->edited ? 'bg-success' : 'bg-primary' ?>"><?= $editedList[$model->edited] ?></span></dd>
        <dt>Status</dt>
        <dd><span class="badge <?= $model->status ? 'bg-success' : 'bg-primary' ?>"><?= $statusList[$model->status] ?></span></dd>
    </dl>

</div>
