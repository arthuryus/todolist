<?php
/* @var $model frontend\models\Task */
/* @var $editedList array */
/* @var $statusList array */
/* @var $action String */
/* @var $isAuth boolean */
?>
<form id="form" action="<?= $action ?>" class="js-form-ajax">
    <div class="form-group mb-3">
        <label class="form-label">Username</label>
        <input type="text" name="<?= $model->formName().'[username]' ?>" class="form-control" value="<?= $model->username ?>">
        <div class="helper-text visually-hidden"></div>
    </div>
    <div class="form-group mb-3">
        <label class="form-label">Email address</label>
        <input type="text" name="<?= $model->formName().'[email]' ?>" class="form-control" value="<?= $model->email ?>">
        <div class="helper-text visually-hidden"></div>
    </div>
    <div class="form-group mb-3">
        <label class="form-label">Text</label>
        <textarea name="<?= $model->formName().'[text]' ?>" class="form-control"><?= $model->text ?></textarea>
        <div class="helper-text visually-hidden"></div>
    </div>
    <? if ($isAuth) { ?>
        <div class="form-group mb-3">
            <label class="form-label">Status</label>
            <select class="form-select" name="<?= $model->formName().'[status]' ?>">
                <? foreach ($statusList as $key => $val) { ?>
                    <option value="<?= $key ?>"<?= $key == $model->status ? ' selected' : '' ?>><?= $val ?></option>
                <? } ?>
            </select>
        </div>
    <? } ?>
    <button type="submit" class="btn btn-primary"><?= $model->id ? 'Update' : 'Create' ?></button>
</form>
