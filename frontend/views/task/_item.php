<?php
/* @var $model frontend\models\Task */
/* @var $index integer */
/* @var $editedList array */
/* @var $statusList array */
/* @var $isAuth boolean */
?>
<tr>
    <td><?= htmlspecialchars($model->username) ?></td>
    <td><?= htmlspecialchars($model->email) ?></td>
    <td><?= htmlspecialchars($model->text) ?></td>
    <? if ($isAuth) { ?>
        <td><span class="badge <?= $model->edited ? 'bg-success' : 'bg-primary' ?>"><?= $editedList[$model->edited] ?></span></td>
    <? } ?>
    <td><span class="badge <?= $model->status ? 'bg-success' : 'bg-primary' ?>"><?= $statusList[$model->status] ?></span></td>
    <td>
        <a href="<?= $model->getUrl('view') ?>">View</a>
        <? if ($isAuth) { ?>
            <a href="<?= $model->getUrl('update') ?>">Update</a>
            <a href="<?= $model->getUrl('delete') ?>">Delete</a>
        <? } ?>
    </td>
</tr>
