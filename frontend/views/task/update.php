<?php
/* @var $model frontend\models\Task */
/* @var $editedList array */
/* @var $statusList array */
/* @var $action String */
/* @var $isAuth boolean */

$this->title = 'Task Update';
?>
<div class="task-update">

    <h1><?= $this->title ?></h1>

    <?= $this->renderContent('task/_form', [
        'model' => $model,
        'editedList' => $editedList,
        'statusList' => $statusList,
        'action' => $action,
        'isAuth' => $isAuth
    ]) ?>

</div>
