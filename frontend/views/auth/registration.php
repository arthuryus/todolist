<?php
/* @var $model frontend\models\RegistrationForm */
/* @var $action String */

$this->title = 'Registration';
?>
<div class="auth-registration">
    <h1><?= $this->title ?></h1>

    <form id="form" action="<?= $action ?>" class="js-form-ajax">
        <div class="form-group mb-3">
            <label class="form-label">Username</label>
            <input type="text" name="<?= $model->formName().'[username]' ?>" class="form-control" value="<?//= $model->username ?>">
            <div class="helper-text visually-hidden"></div>
        </div>
        <div class="form-group mb-3">
            <label class="form-label">Password</label>
            <input type="password" name="<?= $model->formName().'[password]' ?>" class="form-control" value="<?//= $model->email ?>">
            <div class="helper-text visually-hidden"></div>
        </div>
        <div class="form-group mb-3">
            <label class="form-label">Password Confirm</label>
            <input type="password" name="<?= $model->formName().'[password_confirm]' ?>" class="form-control" value="<?//= $model->email ?>">
            <div class="helper-text visually-hidden"></div>
        </div>
        <button type="submit" class="btn btn-primary">Registration</button>
    </form>
</div>
