<?php
/* @var $model frontend\models\LoginForm */
/* @var $action String */

$this->title = 'Login';
?>
<div class="auth-login">
    <h1><?= $this->title ?></h1>

    <form id="form" action="<?= $action ?>" class="js-form-ajax">
        <div class="form-group mb-3">
            <label class="form-label">Username</label>
            <input type="text" name="<?= $model->formName().'[username]' ?>" class="form-control" value="<?//= $model->username ?>">
            <div class="helper-text visually-hidden"></div>
        </div>
        <div class="form-group mb-3">
            <label class="form-label">Password</label>
            <input type="password" name="<?= $model->formName().'[password]' ?>" class="form-control" value="<?//= $model->email ?>">
            <div class="helper-text visually-hidden"></div>
        </div>
        <button type="submit" class="btn btn-primary">Login</button>
    </form>

    <div class="mt-3 text-end">
        <a href="/auth/registration" class="btn btn-primary">Registration</a>
    </div>
</div>
