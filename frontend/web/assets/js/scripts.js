$(function () {

    $('.js-form-ajax').on('submit', function(e){
        e.preventDefault();

        var $form = $(this);

        $.ajax({
            type: 'POST',
            url: $form.attr('action'),
            data: $form.serializeArray(),
            success: function (response) {
                if (response.redirect) {
                    window.location.href = response.redirect;
                }
                else if (response.success) {

                }
                else {
                    $form.find('.form-group').each(function(index, element){
                        $(element).find('input, textarea, select').addClass('is-valid').removeClass('is-invalid');
                        $(element).find('.helper-text').addClass('valid-feedback').removeClass('invalid-feedback').addClass('visually-hidden').text('');
                    });

                    $.each(response.errors, function (key, errors) {
                        $.each(errors, function (i, error) {
                            let element = $form.find('.form-group input[name$="['+key+']"], .form-group textarea[name$="['+key+']"], .form-group select[name$="['+key+']"]');

                            element.addClass('is-invalid').removeClass('is-valid');
                            element.next('.helper-text').addClass('invalid-feedback').removeClass('valid-feedback').removeClass('visually-hidden').text(error);
                        })
                    });
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

            }
        });
    });

});
