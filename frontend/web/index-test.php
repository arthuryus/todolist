<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);

require(__DIR__ . '/../../vendor/autoload.php');

define('DIRECTORY_ROOT', dirname(dirname(__DIR__)) . '/');

$dotEnv = Dotenv\Dotenv::createImmutable(DIRECTORY_ROOT);
$dotEnv->load();

$config = array_merge(
    require(DIRECTORY_ROOT . 'common/config/test.php'),
    require(DIRECTORY_ROOT . 'frontend/config/test.php'),
);

(new \app\base\Application($config))->run();
