<?php
namespace frontend\controllers;

use app\App;
use app\web\Controller;
use app\base\Helper;
use frontend\models\LoginForm;
use frontend\models\RegistrationForm;

/**
 * Class frontend\controllers\AuthController
 */
class AuthController extends Controller
{
    /**
     * @return string
     * @throws \ReflectionException
     */
    public function actionLogin()
    {

        $action = Helper::getUrlFull('/'.App::getConfig('default')['authRoute']);

        $model = new LoginForm();

        if (App::$app->request->isAjax) {
            if ($model->load(App::$app->request->post)) {
                if ($model->validate()) {
                    if ($model->doLogin()) {
                        App::$app->session->setValue('alert', [
                            'type' => 'success',
                            'message' => 'You have successfully logged in!'
                        ]);

                        return $this->renderAjax(['success' => true, 'redirect' => Helper::getUrlFull('/')]);
                    }
                    else {
                        App::$app->session->setValue('alert', [
                            'type' => 'danger',
                            'message' => 'Something went wrong, please try later!'
                        ]);

                        return $this->renderAjax(['success' => false, 'redirect' => $action]);
                    }
                }
                else {
                    return $this->renderAjax(['success' => false, 'errors' => $model->getErrors()]);
                }
            }
        }

        return $this->render('auth/login', [
            'model' => $model,
            'action' => $action
        ]);
    }

    /**
     * @return string.
     */
    public function actionLogout()
    {
        App::$app->auth->logout();

        return $this->redirect('/');
    }

    /**
     * @return string.
     * @throws \ReflectionException
     */
    public function actionRegistration()
    {
        $action = Helper::getUrlFull('/'.'auth/registration');

        $model = new RegistrationForm();

        if (App::$app->request->isAjax) {
            if ($model->load(App::$app->request->post)) {
                if ($model->validate()) {
                    if ($model->doRegistration()) {
                        App::$app->session->setValue('alert', [
                            'type' => 'success',
                            'message' => 'You have successfully create!'
                        ]);

                        return $this->renderAjax(['success' => true, 'redirect' => Helper::getUrlFull('/')]);
                    }
                    else {
                        App::$app->session->setValue('alert', [
                            'type' => 'danger',
                            'message' => 'Something went wrong, please try later!'
                        ]);

                        return $this->renderAjax(['success' => false, 'redirect' => $action]);
                    }
                }
                else {
                    return $this->renderAjax(['success' => false, 'errors' => $model->getErrors()]);
                }
            }
        }

        return $this->render('auth/registration', [
            'model' => $model,
            'action' => $action
        ]);
    }
}
