<?php
namespace frontend\controllers;

use app\web\Controller;

/**
 * Class frontend\controllers\SiteController
 */
class SiteController extends Controller
{
    /**
     * @return string.
     */
    public function actionIndex()
    {
        return $this->render('site/index', ['text' => 'Controller Site method Run TEXT']);
    }

    /**
     * @param string $message
     * @return string.
     */
    public function actionError($message='')
    {
        return $this->render('site/error', ['text' => $message]);
    }
}
