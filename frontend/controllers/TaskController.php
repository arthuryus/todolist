<?php
namespace frontend\controllers;

use app\App;
use app\web\Controller;
use app\base\Helper;
use frontend\models\Task;

/**
 * Class frontend\controllers\TaskController
 */
class TaskController extends Controller
{
    /**
     * @return string.
     */
    public function actionIndex()
    {
        $where = [];//['`status` =? ', [1]];

        $total = Task::getCount($where);
        $page = intval(App::$app->request->get['page']) > 1 ? intval(App::$app->request->get['page']) : 1;
        $perPage = intval(App::$app->request->get['per-page']) > 1 ? intval(App::$app->request->get['per-page']) : 3;
        $sort = in_array(ltrim((string)App::$app->request->get['sort'], '-'), ['username', 'email', 'text', 'edited', 'status']) ? App::$app->request->get['sort'] : '-id';

        $length = ceil($total / $perPage);

        $orderBy = [ltrim($sort, '-') => $sort[0] == '-' ? 'DESC' : 'ASC'];
        $limit = [($page - 1) * $perPage, $perPage];

        $models = Task::findAll($where, $orderBy, $limit);

        return $this->render('task/index', [
            'models' => $models,
            'editedList' => Task::getEditedList(),
            'statusList' => Task::getStatusList(),
            'total' => $total,
            'page' => $page,
            'perPage' => $perPage,
            'sort' => $sort,
            'length' => $length,
            'isAuth' => App::$app->isAuth(),
        ]);
    }

    /**
     * @throws \Exception.
     * @return string.
     */
    public function actionView()
    {
        if (!($model = Task::findOne(['`id` = ?', [intval(App::$app->request->get['id'])]]))) {
            throw new \Exception("Page not found", 404);
        }

        return $this->render('task/view', [
            'model' => $model,
            'editedList' => Task::getEditedList(),
            'statusList' => Task::getStatusList()
        ]);
    }

    /**
     * @return string.
     */
    public function actionCreate()
    {
        $model = new Task();

        if (App::$app->request->isAjax) {
            if ($model->load(App::$app->request->post)) {
                if ($model->validate()) {
                    if ($model->insert()) {
                        App::$app->session->setValue('alert', [
                            'type' => 'success',
                            'message' => 'You have successfully created!'
                        ]);

                        return $this->renderAjax(['success' => true, 'redirect' => Helper::getUrlFull('/')]);
                    }
                    else {
                        App::$app->session->setValue('alert', [
                            'type' => 'danger',
                            'message' => 'Something went wrong, please try later!'
                        ]);

                        return $this->renderAjax(['success' => false, 'redirect' => Helper::getUrlFull('/create')]);
                    }
                }
                else {
                    return $this->renderAjax(['success' => false, 'errors' => $model->getErrors()]);
                }
            }
        }

        return $this->render('task/create', [
            'model' => $model,
            'editedList' => Task::getEditedList(),
            'statusList' => Task::getStatusList(),
            'action' => $model->getUrl('create'),
            'isAuth' => App::$app->isAuth(),
        ]);
    }

    /**
     * @throws \Exception.
     * @return string.
     */
    public function actionUpdate()
    {
        /**
         * @var Task $model
        */
        if (!($model = Task::findOne(['`id` = ?', [intval(App::$app->request->get['id'])]]))) {
            throw new \Exception("Page not found.", 404);
        }

        if (App::$app->request->isAjax) {
            if ($model->load(App::$app->request->post)) {
                if ($model->validate()) {
                    if ($model->update()) {
                        App::$app->session->setValue('alert', [
                            'type' => 'success',
                            'message' => 'You have successfully updated!'
                        ]);

                        return $this->renderAjax(['success' => true, 'redirect' => Helper::getUrlFull('/')]);
                    }
                    else {
                        App::$app->session->setValue('alert', [
                            'type' => 'danger',
                            'message' => 'Something went wrong, please try later!'
                        ]);

                        return $this->renderAjax(['success' => false, 'redirect' => Helper::getUrlFull('/update', ['id' => $model->id])]);
                    }
                }
                else {
                    return $this->renderAjax(['success' => false, 'errors' => $model->getErrors()]);
                }
            }
        }

        return $this->render('task/update', [
            'model' => $model,
            'editedList' => Task::getEditedList(),
            'statusList' => Task::getStatusList(),
            'action' => $model->getUrl('update', ['id' => $model->id]),
            'isAuth' => App::$app->isAuth(),
        ]);
    }

    /**
     * @throws \Exception.
     * @return string.
     */
    public function actionDelete()
    {
        if (!($model = Task::findOne(['`id` = ?', [intval(App::$app->request->get['id'])]]))) {
            throw new \Exception("Page not found.", 404);
        }

        if ($model->delete()) {
            App::$app->session->setValue('alert', [
                'type' => 'success',
                'message' => 'You have successfully deleted!'
            ]);
        }
        else {
            App::$app->session->setValue('alert', [
                'type' => 'danger',
                'message' => 'Something went wrong, please try later!'
            ]);
        }

        return $this->redirect('/');
    }
}
