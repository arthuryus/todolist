<?php
return [
    'default' => [
        'controllerNamespace' => 'frontend\controllers',
        'defaultRoute' => 'task/index',
        'errorRoute' => 'site/error',
        'authRoute' => 'auth/login',
    ],
    'params' => [

    ],
    'router' => [
        '/' => 'task/index',
        '/create' => 'task/create',
        '/update' => ['task/update', true],
        '/delete' => ['task/delete', true],
        '/view' => 'task/view',
        '/auth/login' => 'auth/login',
        '/auth/logout' => ['auth/logout', true],
        '/auth/registration' => 'auth/registration',
    ]
];
