<?php
namespace frontend\models;

use app\base\Helper;

class Task extends \common\models\Task
{
    /**
     * @param string $action
     * @return string
     */
    public function getUrl(string $action):string
    {
        return Helper::getUrl('/'.$action, ['id' => $this->id]);
    }
}
