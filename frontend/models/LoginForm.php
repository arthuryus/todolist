<?php
namespace frontend\models;

use app\App;
use app\base\Helper;
use app\base\Model;
use frontend\models\User;

/**
 * Class frontend\models\LoginForm
 *
 * @property string $username
 * @property string $password
 */
class LoginForm extends Model
{
    public string $username;
    public string $password;

    /**
     * @return bool
     */
    public function validate():bool
    {
        $this->username = Helper::cleanValue($this->username);
        if (!$this->username) {
            $this->setErrors('username', 'Required field «Username»');
        }

        $this->password = Helper::cleanValue($this->password);
        if (!$this->password) {
            $this->setErrors('password', 'Required field «Password»');
        }

        if (!$this->hasErrors()) {
            if ( !($model = User::findByUsername($this->username)) || ($model && !$model->validatePassword($this->password) ) ) {
                $this->setErrors('username', '«Username» is incorrect.');
                $this->setErrors('password', '«Password» is incorrect.');
            }
            elseif (!$model->status) {
                $this->setErrors('username', '«Username» has blocked.');
            }
        }

        return !$this->hasErrors();
    }

    /**
     * @return User
     */
    public function getUser():User
    {
        return User::findByUsername($this->username);
    }

    /**
     * @return bool
     */
    public function doLogin():bool
    {
        return App::$app->auth->login($this->getUser());
    }
}
