<?php
namespace frontend\models;

use app\App;
use app\base\Helper;
use app\base\Model;
use common\models\User;

/**
 * Class frontend\models\RegistrationForm
 *
 * @property string $username
 * @property string $password
 * @property string $password_confirm
 */
class RegistrationForm extends Model
{
    public string $username;
    public string $password;
    public string $password_confirm;

    /**
     * @return bool
     */
    public function validate():bool
    {
        $this->username = Helper::cleanValue($this->username);
        if (!$this->username) {
            $this->setErrors('username', 'Required field «Username»');
        }

        $this->password = Helper::cleanValue($this->password);
        if (!$this->password) {
            $this->setErrors('password', 'Required field «Password»');
        }

        $this->password_confirm = Helper::cleanValue($this->password_confirm);
        if (!$this->password_confirm) {
            $this->setErrors('password_confirm', 'Required field «Password Confirm»');
        }

        if ( !($this->password == $this->password_confirm) ) {
            $this->setErrors('password_confirm', '«Password» and «Password Confirm» do not match.');
        }

        if (!$this->hasErrors()) {
            if ($model = User::findByUsername($this->username)) {
                $this->setErrors('username', '«Username» already exists.');
            }
        }

        return !$this->hasErrors();
    }

    /**
     * @return User
     */
    public function registration():User
    {
        $model = new User();
        $model->setAttributes([
            'username' => $this->username,
            'password' => User::generatePasswordHash($this->password),
            'created_at' => strtotime('now'),
            'status' => 1
        ]);
        $model->insert();

        return $model;
    }

    /**
     * @return bool
     */
    public function doRegistration():bool
    {
        $model = $this->registration();

        return App::$app->auth->login($model);
    }
}
