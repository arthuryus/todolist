<?php
namespace common\models;

use app\base\DatabaseModel;

/**
 * Class common\models\Auth
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property integer $logged_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 */
class User extends DatabaseModel
{
    /**
     * @param string $password
     * @return string
     */
    public static function generatePasswordHash(string $password):string
    {
        return md5($password);
    }

    /**
     * @param string $password
     * @return bool
     */
    public function validatePassword(string $password):bool
    {
        return (static::generatePasswordHash($password) === $this->password);
    }

    /**
     * @param string $username
     * @return User|null
     */
    public static function findByUsername(string $username):?User
    {
        return self::findOne(['`username` = ?', [$username]]);
    }
}
