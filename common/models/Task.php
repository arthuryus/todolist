<?php
namespace common\models;

use app\App;
use app\base\DatabaseModel;
use app\base\Helper;

/**
 * Class common\models\Task
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $text
 * @property integer $edited
 * @property integer $status
 */
class Task extends DatabaseModel
{
    /**
     * @return array
     */
    public static function getEditedList():array
    {
        return [
            0 => 'No',
            1 => 'Yes',
        ];
    }

    /**
     * @return array
     */
    public static function getStatusList():array
    {
        return [
            0 => 'New',
            1 => 'Completed',
        ];
    }

    /**
     * @return bool
     */
    public function validate():bool
    {
        $this->username = Helper::cleanValue($this->username);
        if (!$this->username) {
            $this->setErrors('username', 'Required field «Username»');
        }

        $this->email = Helper::cleanValue($this->email);
        if (!$this->email) {
            $this->setErrors('email', 'Required field «Email»');
        }
        elseif (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->setErrors('email', '«Email» is incorrect.');
        }

        $this->text = Helper::cleanValue($this->text);
        if (!$this->text) {
            $this->setErrors('text', 'Required field «Text»');
        }


        $this->edited = intval($this->id ? ( ($this->text != $this->getOldAttributes()['text']) ? 1 : $this->getOldAttributes()['edited'] ) : 0);
        $this->status = intval(App::$app->isAuth() ? ($this->status ? 1 : 0) : ($this->id ? $this->getOldAttributes()['status'] : 0));

        return !$this->hasErrors();
    }
}
