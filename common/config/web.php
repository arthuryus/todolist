<?php
return [
    'db' => [
        'dsn' => $_ENV['DB_DSN'],
        'username' => $_ENV['DB_USERNAME'],
        'password' => $_ENV['DB_PASSWORD'],
        'charset' => $_ENV['DB_CHARSET'],
    ],
    'containers' => [
        'AuthService' => ['class' => frontend\services\auth\AuthService::class],
        'TaskService' => ['class' => frontend\services\task\TaskService::class],
    ]
];
