<?php
return [
    'db' => [
        'dsn' => $_ENV['TEST_DB_DSN'],
        'username' => $_ENV['TEST_DB_USERNAME'],
        'password' => $_ENV['TEST_DB_PASSWORD'],
        'charset' => $_ENV['TEST_DB_CHARSET'],
    ],
    'containers' => [
        'AuthService' => ['class' => frontend\services\auth\AuthService::class],
        'TaskService' => ['class' => frontend\services\task\TaskService::class],
    ]
];
